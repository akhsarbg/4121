\BOOKMARK [1][-]{section.1}{Fundamental Concepts in Testing}{}% 1
\BOOKMARK [2][-]{subsection.1.1}{What can SW system failures lead to?}{section.1}% 2
\BOOKMARK [2][-]{subsection.1.2}{What causes software defects?}{section.1}% 3
\BOOKMARK [2][-]{subsection.1.3}{What’s the relationship between failures and defects?}{section.1}% 4
\BOOKMARK [2][-]{subsection.1.4}{What is the definition of testing?}{section.1}% 5
\BOOKMARK [2][-]{subsection.1.5}{What is the role of testing?}{section.1}% 6
\BOOKMARK [2][-]{subsection.1.6}{Amount of testing depends on what?}{section.1}% 7
\BOOKMARK [2][-]{subsection.1.7}{What are the 7 principles of testing?}{section.1}% 8
\BOOKMARK [2][-]{subsection.1.8}{What’s the 5 fundamental test activities?}{section.1}% 9
\BOOKMARK [2][-]{subsection.1.9}{What kind of independence test levels do we have?}{section.1}% 10
\BOOKMARK [1][-]{section.2}{Testing Throughout the software life cycle}{}% 11
\BOOKMARK [2][-]{subsection.2.1}{What is the sequential development model?}{section.2}% 12
\BOOKMARK [2][-]{subsection.2.2}{What is the iterative incremental development model}{section.2}% 13
\BOOKMARK [2][-]{subsection.2.3}{How does testing work in life cycle models?}{section.2}% 14
\BOOKMARK [2][-]{subsection.2.4}{What are the different levels of testing?}{section.2}% 15
\BOOKMARK [2][-]{subsection.2.5}{What’s acceptance testing?}{section.2}% 16
\BOOKMARK [2][-]{subsection.2.6}{What’s system testing?}{section.2}% 17
\BOOKMARK [2][-]{subsection.2.7}{What is integration testing?}{section.2}% 18
\BOOKMARK [2][-]{subsection.2.8}{What’s component testing?}{section.2}% 19
\BOOKMARK [2][-]{subsection.2.9}{What test types do we have?}{section.2}% 20
\BOOKMARK [2][-]{subsection.2.10}{What is functional testing?}{section.2}% 21
\BOOKMARK [2][-]{subsection.2.11}{What is non functional testing?}{section.2}% 22
\BOOKMARK [2][-]{subsection.2.12}{What is structural Testing?}{section.2}% 23
\BOOKMARK [2][-]{subsection.2.13}{What is maintenance testing?}{section.2}% 24
\BOOKMARK [1][-]{section.3}{Static Techniques}{}% 25
\BOOKMARK [2][-]{subsection.3.1}{What is the difference between dynamic and static testing?}{section.3}% 26
\BOOKMARK [2][-]{subsection.3.2}{What are reviews, and why do we do reviews?}{section.3}% 27
\BOOKMARK [2][-]{subsection.3.3}{What types of reviews do we have? And what are the differences?}{section.3}% 28
\BOOKMARK [2][-]{subsection.3.4}{What are the phases of a formal review?}{section.3}% 29
\BOOKMARK [2][-]{subsection.3.5}{What are the roles and responsibilies in a review?}{section.3}% 30
\BOOKMARK [2][-]{subsection.3.6}{What are the different types of reviews?}{section.3}% 31
\BOOKMARK [2][-]{subsection.3.7}{What are the success factors for reviews?}{section.3}% 32
\BOOKMARK [2][-]{subsection.3.8}{What are the objectives of static analysis?}{section.3}% 33
\BOOKMARK [1][-]{section.4}{Test Design Techniques}{}% 34
\BOOKMARK [2][-]{subsection.4.1}{What is a test condition?}{section.4}% 35
\BOOKMARK [2][-]{subsection.4.2}{What does a test case consist of?}{section.4}% 36
\BOOKMARK [2][-]{subsection.4.3}{What should expected result include?}{section.4}% 37
\BOOKMARK [2][-]{subsection.4.4}{How are test implementation of test cases done?}{section.4}% 38
\BOOKMARK [2][-]{subsection.4.5}{What does test execution schedule define?}{section.4}% 39
\BOOKMARK [2][-]{subsection.4.6}{Recall the purpose of both black-box testing and white-box testing. Give example of techniques for each type of technique.}{section.4}% 40
\BOOKMARK [2][-]{subsection.4.7}{ Explain the characteristics, differences and cases in which to use black-box, white-box and experience based testing techniques}{section.4}% 41
\BOOKMARK [2][-]{subsection.4.8}{What is equivalence partitioning?}{section.4}% 42
\BOOKMARK [2][-]{subsection.4.9}{What is boundary value analysis?}{section.4}% 43
\BOOKMARK [2][-]{subsection.4.10}{What is decision table testing?}{section.4}% 44
\BOOKMARK [2][-]{subsection.4.11}{What is state transition testing?}{section.4}% 45
\BOOKMARK [2][-]{subsection.4.12}{What is use case testing}{section.4}% 46
\BOOKMARK [2][-]{subsection.4.13}{What structures can be tested in white box?}{section.4}% 47
\BOOKMARK [2][-]{subsection.4.14}{What is statement testing?}{section.4}% 48
\BOOKMARK [2][-]{subsection.4.15}{What is decision testing?}{section.4}% 49
\BOOKMARK [2][-]{subsection.4.16}{What is experience based testing?}{section.4}% 50
\BOOKMARK [2][-]{subsection.4.17}{Choosing test techniques}{section.4}% 51
\BOOKMARK [1][-]{section.5}{Test Mangement}{}% 52
\BOOKMARK [2][-]{subsection.5.1}{What are the tasks of the test leader?}{section.5}% 53
\BOOKMARK [2][-]{subsection.5.2}{What are the tasks of the tester?}{section.5}% 54
\BOOKMARK [2][-]{subsection.5.3}{What is test planning?}{section.5}% 55
\BOOKMARK [2][-]{subsection.5.4}{What is the entry criteria and what does it consist of?}{section.5}% 56
\BOOKMARK [2][-]{subsection.5.5}{What is the exit criteria and what doe sit consist of?}{section.5}% 57
\BOOKMARK [2][-]{subsection.5.6}{What is test estimation?}{section.5}% 58
\BOOKMARK [2][-]{subsection.5.7}{What test approaches do we have?}{section.5}% 59
\BOOKMARK [2][-]{subsection.5.8}{What is test progress monitoring?}{section.5}% 60
\BOOKMARK [2][-]{subsection.5.9}{What is test reporting?}{section.5}% 61
\BOOKMARK [2][-]{subsection.5.10}{What is test control?}{section.5}% 62
\BOOKMARK [2][-]{subsection.5.11}{What is configuration management?}{section.5}% 63
\BOOKMARK [2][-]{subsection.5.12}{What is risk, and how do we determine the level of risk?}{section.5}% 64
\BOOKMARK [2][-]{subsection.5.13}{What is incident management?}{section.5}% 65
\BOOKMARK [2][-]{subsection.5.14}{What is project risks?}{section.5}% 66
\BOOKMARK [2][-]{subsection.5.15}{What is product risk?}{section.5}% 67
\BOOKMARK [1][-]{section.6}{TOOLS SUPPORT FOR TESTING}{}% 68
\BOOKMARK [2][-]{subsection.6.1}{What types of test tools do we have?}{section.6}% 69
\BOOKMARK [2][-]{subsection.6.2}{Why do we use test tools?}{section.6}% 70
\BOOKMARK [2][-]{subsection.6.3}{What are requirement management tools?}{section.6}% 71
\BOOKMARK [2][-]{subsection.6.4}{What are incidient management tools?}{section.6}% 72
\BOOKMARK [2][-]{subsection.6.5}{What are configuration management tools?}{section.6}% 73
\BOOKMARK [2][-]{subsection.6.6}{How do we introduce a test tool into an organization}{section.6}% 74
\BOOKMARK [1][-]{section.7}{HCI BROR}{}% 75
\BOOKMARK [2][-]{subsection.7.1}{What is HCI?}{section.7}% 76
\BOOKMARK [2][-]{subsection.7.2}{What is the purpose of HCI?}{section.7}% 77
\BOOKMARK [2][-]{subsection.7.3}{What does the framework of HCI consist of?}{section.7}% 78
\BOOKMARK [2][-]{subsection.7.4}{What is Interface Standard?}{section.7}% 79
\BOOKMARK [2][-]{subsection.7.5}{What is Usability?}{section.7}% 80
\BOOKMARK [2][-]{subsection.7.6}{What is interface dynamics?}{section.7}% 81
\BOOKMARK [2][-]{subsection.7.7}{What is aesthetics?}{section.7}% 82
\BOOKMARK [2][-]{subsection.7.8}{What is user centric design process?}{section.7}% 83
\BOOKMARK [2][-]{subsection.7.9}{How do we observe users?}{section.7}% 84
\BOOKMARK [2][-]{subsection.7.10}{What are personas?}{section.7}% 85
\BOOKMARK [2][-]{subsection.7.11}{What are prototypes}{section.7}% 86
\BOOKMARK [2][-]{subsection.7.12}{What type of prototypes do we have}{section.7}% 87
\BOOKMARK [2][-]{subsection.7.13}{What are evaluate concepts?}{section.7}% 88
\BOOKMARK [2][-]{subsection.7.14}{What are the purposes of guidelines?}{section.7}% 89
\BOOKMARK [2][-]{subsection.7.15}{What are system messages}{section.7}% 90
\BOOKMARK [1][-]{section.8}{Accessibility Testing}{}% 91
\BOOKMARK [2][-]{subsection.8.1}{What is disability?}{section.8}% 92
\BOOKMARK [2][-]{subsection.8.2}{What is usability?}{section.8}% 93
\BOOKMARK [2][-]{subsection.8.3}{What is accessbility?}{section.8}% 94
\BOOKMARK [2][-]{subsection.8.4}{What are barriers in accessibility, and how are they ranged?}{section.8}% 95
\BOOKMARK [2][-]{subsection.8.5}{Mention 5 personas of accessability study}{section.8}% 96
\BOOKMARK [2][-]{subsection.8.6}{What is accessible design and its 7 points?}{section.8}% 97
\BOOKMARK [2][-]{subsection.8.7}{What does WCAG stand for, and what is it?}{section.8}% 98
\BOOKMARK [2][-]{subsection.8.8}{What is its 4 principles?}{section.8}% 99
\BOOKMARK [2][-]{subsection.8.9}{Mention some assistive technologies}{section.8}% 100
\BOOKMARK [1][-]{section.9}{Exploratory Testing}{}% 101
\BOOKMARK [2][-]{subsection.9.1}{What’s Kaners definiton of a computer program?}{section.9}% 102
\BOOKMARK [2][-]{subsection.9.2}{What is the alternative definition of quality?}{section.9}% 103
\BOOKMARK [2][-]{subsection.9.3}{What is the difference between testing and checking?}{section.9}% 104
\BOOKMARK [2][-]{subsection.9.4}{How do we test the customers expectations?}{section.9}% 105
\BOOKMARK [2][-]{subsection.9.5}{How do we build quality?}{section.9}% 106
\BOOKMARK [2][-]{subsection.9.6}{Mention some context dependent testing}{section.9}% 107
\BOOKMARK [2][-]{subsection.9.7}{What heuristic is????}{section.9}% 108
\BOOKMARK [2][-]{subsection.9.8}{Mention some heuristics and their triggers}{section.9}% 109
\BOOKMARK [2][-]{subsection.9.9}{What is the mission of testing}{section.9}% 110
\BOOKMARK [2][-]{subsection.9.10}{What will testers explore}{section.9}% 111
\BOOKMARK [2][-]{subsection.9.11}{What are mental models}{section.9}% 112
\BOOKMARK [1][-]{section.10}{Test Automation and Test Automation Frameworks}{}% 113
\BOOKMARK [2][-]{subsection.10.1}{What’s Test Automation?}{section.10}% 114
\BOOKMARK [2][-]{subsection.10.2}{What interfaces for automated tests do we have?}{section.10}% 115
\BOOKMARK [2][-]{subsection.10.3}{What are the advantages and limitations of test automation?}{section.10}% 116
\BOOKMARK [2][-]{subsection.10.4}{What are MAJOR KEYS for test automation?}{section.10}% 117
\BOOKMARK [2][-]{subsection.10.5}{What approaches can we use when creating automated tests?}{section.10}% 118
\BOOKMARK [2][-]{subsection.10.6}{What is the capture replay approach?}{section.10}% 119
\BOOKMARK [2][-]{subsection.10.7}{What is the data driven approach?}{section.10}% 120
\BOOKMARK [2][-]{subsection.10.8}{What is the keyword driven approach?}{section.10}% 121
\BOOKMARK [2][-]{subsection.10.9}{What is the test generation layer?}{section.10}% 122
\BOOKMARK [2][-]{subsection.10.10}{What is the test definition layer?}{section.10}% 123
\BOOKMARK [2][-]{subsection.10.11}{What is the test execution layer?}{section.10}% 124
\BOOKMARK [2][-]{subsection.10.12}{What is the test adaption layer?}{section.10}% 125
\BOOKMARK [2][-]{subsection.10.13}{How do we automate manual tests? \(transition from manual to auto\)}{section.10}% 126
\BOOKMARK [1][-]{section.11}{MOBILE SOFTWARE TESTING}{}% 127
\BOOKMARK [2][-]{subsection.11.1}{Mention some of the mobile device categories we have}{section.11}% 128
\BOOKMARK [2][-]{subsection.11.2}{What are the difference we should think about when it comes to tablet/phone sepcific categories?}{section.11}% 129
\BOOKMARK [2][-]{subsection.11.3}{What are mobile phone tasks?}{section.11}% 130
\BOOKMARK [2][-]{subsection.11.4}{What are tabletes task?}{section.11}% 131
\BOOKMARK [2][-]{subsection.11.5}{What are typical behavior for mobile phone users?}{section.11}% 132
\BOOKMARK [2][-]{subsection.11.6}{What are typical behavior for tablet users?}{section.11}% 133
\BOOKMARK [2][-]{subsection.11.7}{Mention some design issues for mobile devices}{section.11}% 134
\BOOKMARK [2][-]{subsection.11.8}{Why are download times when it comes to apps so important?}{section.11}% 135
\BOOKMARK [2][-]{subsection.11.9}{Why should we focus on scrolling when it comes to apps?}{section.11}% 136
\BOOKMARK [2][-]{subsection.11.10}{What problem are often related to zooming?}{section.11}% 137
\BOOKMARK [2][-]{subsection.11.11}{Mention some do’s in mobile design and testing}{section.11}% 138
\BOOKMARK [2][-]{subsection.11.12}{Mention some donts in mobile design and testing}{section.11}% 139
\BOOKMARK [2][-]{subsection.11.13}{How to make mobile optimized apps and sites}{section.11}% 140
\BOOKMARK [1][-]{section.12}{Intellectual Propery and Licensicng}{}% 141
\BOOKMARK [2][-]{subsection.12.1}{What does IP stand for in testing contexts?}{section.12}% 142
\BOOKMARK [2][-]{subsection.12.2}{What are the categories of IP?}{section.12}% 143
\BOOKMARK [2][-]{subsection.12.3}{What are tangible and intangible properies?}{section.12}% 144
\BOOKMARK [2][-]{subsection.12.4}{Why give IP protection, and what can be done with it?}{section.12}% 145
\BOOKMARK [2][-]{subsection.12.5}{What are trade secrets?}{section.12}% 146
\BOOKMARK [2][-]{subsection.12.6}{What are trademarks?}{section.12}% 147
\BOOKMARK [2][-]{subsection.12.7}{What are patents?}{section.12}% 148
\BOOKMARK [2][-]{subsection.12.8}{What are industrial design?}{section.12}% 149
\BOOKMARK [2][-]{subsection.12.9}{What are geopgrahical indications?}{section.12}% 150
\BOOKMARK [2][-]{subsection.12.10}{What is confidential information?}{section.12}% 151
\BOOKMARK [2][-]{subsection.12.11}{What is copy right?}{section.12}% 152
\BOOKMARK [2][-]{subsection.12.12}{What are public domains?}{section.12}% 153
\BOOKMARK [2][-]{subsection.12.13}{What is fair use?}{section.12}% 154
\BOOKMARK [2][-]{subsection.12.14}{What is misuse of sources?}{section.12}% 155
\BOOKMARK [2][-]{subsection.12.15}{What is a license, and why do we need them?}{section.12}% 156
\BOOKMARK [2][-]{subsection.12.16}{What’s the relation between IP and licenses?}{section.12}% 157
\BOOKMARK [2][-]{subsection.12.17}{What does a license give you the right for?}{section.12}% 158
\BOOKMARK [2][-]{subsection.12.18}{What types of licenses do we have?}{section.12}% 159
\BOOKMARK [2][-]{subsection.12.19}{Give some reasons on why to license and rent license}{section.12}% 160
\BOOKMARK [2][-]{subsection.12.20}{What do we need to test regarding licenses?}{section.12}% 161
