package primes;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ Test_getPrimeUpTo.class, Test_isPrime.class })
public class AllTests {

}
