package primes;

import static org.junit.Assert.*;

import org.junit.Test;

public class Test_isPrime {

	@Test
	public void test_isPrime() {
		Primes p = new Primes();
		int[] values = {-5, 0, 1, 2, 3, 4, 7, 8, 13, 25, 37, 99};
		boolean[] results = {false, false, false, true, true, false, true, false, true, false, true, false};
		
		for(int i=0; i<values.length; i++) {
			boolean result = p.isPrime(values[i]);
			boolean expected = results[i];
			assertEquals(expected, result);
		}

	}

}
