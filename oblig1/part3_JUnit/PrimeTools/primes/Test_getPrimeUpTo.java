package primes;

import static org.junit.Assert.*;

import org.junit.Test;

public class Test_getPrimeUpTo {

	@Test
	public void test_getPrimeUpTo() {
		Primes p = new Primes();
		boolean[] result = p.getPrimesUpTo(10);
		boolean[] expected = {true, true, true, true, false, true, false, true, false, false};
		assertArrayEquals(expected, result);
	}

}
