package primes;

import java.util.Arrays;

public class Primes {
	public boolean[] getPrimesUpTo(int upper_limit) {
		if(upper_limit < 2)
			return new boolean[0];
		
		boolean[] temp = new boolean[upper_limit];
		Arrays.fill(temp, true);
		
		for(int i=2; i<upper_limit; i++)
			for(int k=i; k<upper_limit; k+=i)
				if(k!=i && temp[k] != false)
					temp[k] = false;

		return temp;
	}
	
	public boolean isPrime(int number) {
		if(number < 2) return false;
		
		boolean[] temp = getPrimesUpTo(number+1);
		return temp[number] == true;
	}
}
