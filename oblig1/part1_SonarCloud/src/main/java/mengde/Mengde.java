package mengde;

class Mengde<E> {
    private Node<E> fremst, sist;

    //Legger til Node på slutten av listen
    //Hvis liste er tom er fremst == sist
    //Returnerer true hvis noden ble lagt til, false ellers
    public boolean leggTil(E e){
	if(inneholder(e))
	    return false;
	Node<E> n = new Node<E>(e);
	if(tom()){
	    fremst = sist = n;
	}else{
	    sist.neste = n;
	    sist = n;
	}
	return true;
    }

    public E fjernEldste(){
	if(tom())  return null;
	Node<E> n = fremst;
	//Hvis liste inneholder bare en node
	if(fremst == sist){
	    fremst = sist = null;
	}else{
	    fremst = fremst.neste;
	}
	return n.data;
    }

    //Fjerner noden vi la inn sist
    public E fjernNyeste(){
	//Returnerer null påtom liste
	if(tom()) return null;
	Node<E> n = fremst;
	//Hvis liste inneholder bare en node
	if(fremst == sist){
	    fremst = sist = null;
	}else{
	    Node<E> temp = fremst;
	    //Loop for å finne nest siste noden
	    while(temp != null){
		if(temp.neste == sist)
		    break;
		temp = temp.neste;
	    }
	    n = temp.neste;
	    temp.neste = null;
	    sist = temp;
	}
	return n.data;
    }
    //Sjekker om noden finnes fra før
    public boolean inneholder(E e){
	if(tom()) return false;
	Node<E> n = fremst;
	while(n != null){
	    if(n.data == e)
		return true;
	    n = n.neste;
	}
	return false;
    }
    //Sjekker om lista er tom
    public boolean tom(){
	//funker med både fremst og sist
	return fremst == null;
    }
    
    //Konteiner for data vi skal lagre i listen
    private class Node<T> {
	T data;
	Node<E> neste = null;

	Node(T t) {
	    data = t;
	}
    }
}
